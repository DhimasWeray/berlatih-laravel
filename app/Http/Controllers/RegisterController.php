<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function index(){
        return view('index');
    }
    
    public function form(){
        return view('form');
    }

    public function welcome(){
        return view('welcome');
    }
    public function welcome_post(Request $request){
      $fname = $request["fname"];
      $lname = $request["lname"];

      return view('welcome',['fname' => $fname],['lname' => $lname]);
    }



}